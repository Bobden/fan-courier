package com.courier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FanCourierApplication {

	public static void main(String[] args) {
		SpringApplication.run(FanCourierApplication.class, args);
	}

}
